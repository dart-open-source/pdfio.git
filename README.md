# Pdf  library for dart/flutter


# Example Parse PDF objects

- example [example/pack.dart](example/pack.dart) :

```dart
import 'package:alm/alm.dart';
import 'package:pdfio/pack.dart';

void main() {
  Alm.timer();
  var pack = Pack(Alm.fileBytes('res/test.pdf'));
  pack.decode();
  print(Alm.prettyJson(pack.dicts));
  Alm.timerDiff();
}
```

- if object has Stream data see Byte position 

```json
{
    "/Indirect": "4 0 R",
    "/Length": "5 0 R",
    "/Filter": "/FlateDecode",
    "/Stream": "77->start of stream,508->end of stream" 
}
```

- output: with in (0:00:00.050000)
```json
[
  {
    "/Type": "/Comment",
    "/": "PDF-1.3"
  },
  {
    "/Type": "/Comment",
    "/": "Äåòåë§ó ÐÄÆ"
  },
  {
    "/Indirect": "4 0 R",
    "/Length": "5 0 R",
    "/Filter": "/FlateDecode",
    "/Stream": "77,508"
  },
  {
    "/Indirect": "5 0 R",
    "/": 419
  },
  {
    "/Indirect": "2 0 R",
    "/Type": "/Page",
    "/Parent": "3 0 R",
    "/Resources": "6 0 R",
    "/Contents": "4 0 R",
    "/MediaBox": [
      "0",
      "0",
      "612",
      "792"
    ]
  },
  {
    "/Indirect": "6 0 R",
    "/ProcSet": [
      "/PDF",
      "/Text"
    ],
    "/ColorSpace": {
      "/Cs1": "7 0 R"
    },
    "/Font": {
      "/TT2": "9 0 R",
      "/TT1": "8 0 R",
      "/TT3": "10 0 R"
    }
  },
  {
    "/Indirect": "12 0 R",
    "/Length": "13 0 R",
    "/N": "3",
    "/Alternate": "/DeviceRGB",
    "/Filter": "/FlateDecode",
    "/Stream": "842,3466"
  },
  {
    "/Indirect": "13 0 R",
    "/": 2612
  },
  {
    "/Indirect": "7 0 R",
    "/Info": [
      "/ICCBased",
      "12 0 R"
    ]
  },
  {
    "/Indirect": "15 0 R",
    "/Length": "16 0 R",
    "/Filter": "/FlateDecode",
    "/Stream": "3587,3920"
  },
  {
    "/Indirect": "16 0 R",
    "/": 321
  },
  {
    "/Indirect": "14 0 R",
    "/Type": "/Page",
    "/Parent": "3 0 R",
    "/Resources": "17 0 R",
    "/Contents": "15 0 R",
    "/MediaBox": [
      "0",
      "0",
      "612",
      "792"
    ]
  },
  {
    "/Indirect": "17 0 R",
    "/ProcSet": [
      "/PDF",
      "/Text"
    ],
    "/ColorSpace": {
      "/Cs1": "7 0 R"
    },
    "/Font": {
      "/TT2": "9 0 R",
      "/TT1": "8 0 R",
      "/TT3": "10 0 R"
    }
  },
  {
    "/Indirect": "20 0 R",
    "/Type": "/StructTreeRoot",
    "/K": "19 0 R"
  },
  {
    "/Indirect": "19 0 R",
    "/Type": "/StructElem",
    "/S": "/Document",
    "/P": "20 0 R",
    "/K": [
      "21 0 R",
      "22 0 R",
      "23 0 R",
      "24 0 R",
      "25 0 R",
      "26 0 R",
      "27 0 R",
      "28 0 R"
    ]
  },
  {
    "/Indirect": "21 0 R",
    "/Type": "/StructElem",
    "/S": "/P",
    "/P": "19 0 R",
    "/Pg": "2 0 R",
    "/K": "1"
  },
  {
    "/Indirect": "22 0 R",
    "/Type": "/StructElem",
    "/S": "/H2",
    "/P": "19 0 R",
    "/Pg": "2 0 R",
    "/K": "2"
  },
  {
    "/Indirect": "23 0 R",
    "/Type": "/StructElem",
    "/S": "/P",
    "/P": "19 0 R",
    "/Pg": "2 0 R",
    "/K": "3"
  },
  {
    "/Indirect": "24 0 R",
    "/Type": "/StructElem",
    "/S": "/P",
    "/P": "19 0 R",
    "/Pg": "2 0 R",
    "/K": "4"
  },
  {
    "/Indirect": "25 0 R",
    "/Type": "/StructElem",
    "/S": "/P",
    "/P": "19 0 R",
    "/Pg": "2 0 R",
    "/K": "5"
  },
  {
    "/Indirect": "26 0 R",
    "/Type": "/StructElem",
    "/S": "/P",
    "/P": "19 0 R",
    "/Pg": "14 0 R",
    "/K": "6"
  },
  {
    "/Indirect": "27 0 R",
    "/Type": "/StructElem",
    "/S": "/H2",
    "/P": "19 0 R",
    "/Pg": "14 0 R",
    "/K": "7"
  },
  {
    "/Indirect": "28 0 R",
    "/Type": "/StructElem",
    "/S": "/P",
    "/P": "19 0 R",
    "/Pg": "14 0 R",
    "/K": "8"
  },
  {
    "/Indirect": "3 0 R",
    "/Type": "/Pages",
    "/MediaBox": [
      "0",
      "0",
      "612",
      "792"
    ],
    "/Count": "2",
    "/Kids": [
      "2 0 R",
      "14 0 R"
    ]
  },
  {
    "/Indirect": "29 0 R",
    "/Type": "/Catalog",
    "/Pages": "3 0 R",
    "/MarkInfo": {
      "/Marked": "true"
    },
    "/StructTreeRoot": "20 0 R"
  },
  {
    "/Indirect": "11 0 R",
    "/Info": [
      "2 0 R",
      "/XYZ",
      "0",
      "792",
      "0"
    ]
  },
  {
    "/Indirect": "18 0 R",
    "/Info": [
      "14 0 R",
      "/XYZ",
      "0",
      "1584",
      "0"
    ]
  },
  {
    "/Indirect": "8 0 R",
    "/Type": "/Font",
    "/Subtype": "/TrueType",
    "/BaseFont": "/XSPRIE+HelveticaNeue",
    "/FontDescriptor": "30 0 R",
    "/Encoding": "/MacRomanEncoding",
    "/FirstChar": "32",
    "/LastChar": "120",
    "/Widths": [
      "278",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "556",
      "556",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "278",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "685",
      "0",
      "0",
      "0",
      "0",
      "759",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "648",
      "0",
      "685",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "537",
      "0",
      "537",
      "593",
      "537",
      "0",
      "574",
      "556",
      "222",
      "0",
      "0",
      "222",
      "0",
      "556",
      "574",
      "0",
      "0",
      "333",
      "500",
      "315",
      "556",
      "0",
      "0",
      "518"
    ]
  },
  {
    "/Indirect": "30 0 R",
    "/Type": "/FontDescriptor",
    "/FontName": "/XSPRIE+HelveticaNeue",
    "/Flags": "32",
    "/FontBBox": [
      "-951",
      "-481",
      "1987",
      "1077"
    ],
    "/ItalicAngle": "0",
    "/Ascent": "952",
    "/Descent": "-213",
    "/CapHeight": "714",
    "/StemV": "95",
    "/Leading": "28",
    "/XHeight": "517",
    "/StemH": "80",
    "/AvgWidth": "447",
    "/MaxWidth": "2225",
    "/FontFile2": "31 0 R"
  },
  {
    "/Indirect": "31 0 R",
    "/Length": "32 0 R",
    "/Length1": "6792",
    "/Filter": "/FlateDecode",
    "/Stream": "5945,9996"
  },
  {
    "/Indirect": "32 0 R",
    "/": 4039
  },
  {
    "/Indirect": "10 0 R",
    "/Type": "/Font",
    "/Subtype": "/TrueType",
    "/BaseFont": "/CHSZBQ+HelveticaNeue",
    "/FontDescriptor": "33 0 R",
    "/ToUnicode": "34 0 R",
    "/FirstChar": "33",
    "/LastChar": "33",
    "/Widths": [
      "278"
    ]
  },
  {
    "/Indirect": "34 0 R",
    "/Length": "35 0 R",
    "/Filter": "/FlateDecode",
    "/Stream": "10250,10484"
  },
  {
    "/Indirect": "35 0 R",
    "/": 222
  },
  {
    "/Indirect": "33 0 R",
    "/Type": "/FontDescriptor",
    "/FontName": "/CHSZBQ+HelveticaNeue",
    "/Flags": "4",
    "/FontBBox": [
      "-951",
      "-481",
      "1987",
      "1077"
    ],
    "/ItalicAngle": "0",
    "/Ascent": "952",
    "/Descent": "-213",
    "/CapHeight": "714",
    "/StemV": "95",
    "/Leading": "28",
    "/XHeight": "517",
    "/StemH": "80",
    "/AvgWidth": "447",
    "/MaxWidth": "2225",
    "/FontFile2": "36 0 R"
  },
  {
    "/Indirect": "36 0 R",
    "/Length": "37 0 R",
    "/Length1": "1784",
    "/Filter": "/FlateDecode",
    "/Stream": "10847,11723"
  },
  {
    "/Indirect": "37 0 R",
    "/": 864
  },
  {
    "/Indirect": "9 0 R",
    "/Type": "/Font",
    "/Subtype": "/TrueType",
    "/BaseFont": "/TPTKVJ+HelveticaNeue-Bold",
    "/FontDescriptor": "38 0 R",
    "/Encoding": "/MacRomanEncoding",
    "/FirstChar": "32",
    "/LastChar": "116",
    "/Widths": [
      "278",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "556",
      "556",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "667",
      "0",
      "0",
      "0",
      "611",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "0",
      "574",
      "0",
      "0",
      "0",
      "574",
      "0",
      "611",
      "0",
      "258",
      "0",
      "0",
      "0",
      "0",
      "593",
      "0",
      "611",
      "0",
      "389",
      "537",
      "352"
    ]
  },
  {
    "/Indirect": "38 0 R",
    "/Type": "/FontDescriptor",
    "/FontName": "/TPTKVJ+HelveticaNeue-Bold",
    "/Flags": "32",
    "/FontBBox": [
      "-1018",
      "-481",
      "1437",
      "1141"
    ],
    "/ItalicAngle": "0",
    "/Ascent": "975",
    "/Descent": "-217",
    "/CapHeight": "714",
    "/StemV": "157",
    "/Leading": "29",
    "/XHeight": "517",
    "/StemH": "132",
    "/AvgWidth": "478",
    "/MaxWidth": "1500",
    "/FontFile2": "39 0 R"
  },
  {
    "/Indirect": "39 0 R",
    "/Length": "40 0 R",
    "/Length1": "4736",
    "/Filter": "/FlateDecode",
    "/Stream": "12473,15305"
  },
  {
    "/Indirect": "40 0 R",
    "/": 2820
  },
  {
    "/Indirect": "1 0 R",
    "/Title": "(test)",
    "/Producer": "(macOS Version 10.15.4 \\(Build 19E287\\) Quartz PDFContext)",
    "/Creator": "(Pages)",
    "/CreationDate": "(D:20200519054703Z00'00')",
    "/ModDate": "(D:20200519054703Z00'00')"
  },
  {
    "/Type": "/XRef",
    "/": [
      "0,0,65535,f",
      "1,15333,0,n",
      "2,534,0,n",
      "3,4937,0,n",
      "4,22,0,n",
      "5,515,0,n",
      "6,638,0,n",
      "7,3494,0,n",
      "8,5209,0,n",
      "9,11750,0,n",
      "10,10024,0,n",
      "11,5129,0,n",
      "12,758,0,n",
      "13,3473,0,n",
      "14,3947,0,n",
      "15,3530,0,n",
      "16,3927,0,n",
      "17,4054,0,n",
      "18,5168,0,n",
      "19,4229,0,n",
      "20,4175,0,n",
      "21,4356,0,n",
      "22,4428,0,n",
      "23,4501,0,n",
      "24,4573,0,n",
      "25,4645,0,n",
      "26,4717,0,n",
      "27,4790,0,n",
      "28,4864,0,n",
      "29,5027,0,n",
      "30,5608,0,n",
      "31,5874,0,n",
      "32,10003,0,n",
      "33,10511,0,n",
      "34,10193,0,n",
      "35,10491,0,n",
      "36,10776,0,n",
      "37,11730,0,n",
      "38,12128,0,n",
      "39,12402,0,n",
      "40,15312,0,n"
    ]
  },
  {
    "/Type": "/Trailer",
    "/Size": "41",
    "/Root": "29 0 R",
    "/Info": "1 0 R",
    "/ID": [
      "<53b3f29fa9ab0650336e2064c9796d9d>",
      "<53b3f29fa9ab0650336e2064c9796d9d>"
    ]
  },
  {
    "/Type": "/StartXRef",
    "/": 15529
  },
  {
    "/Type": "/Comment",
    "/": "%EOF"
  }
]
```

