import 'package:alm/alm.dart';
import 'package:pdfio/pack.dart';

void main() {
  var t=Alm.duration;

  var target='dd1';
  var bytes = Alm.file('/Users/alm/Documents/pdf/$target.pdf').tryBytes();

  var pack = Pack(bytes);

  pack.decode();

  pack.dicts.printPretty();

  var enpack = pack.encode({'image':{'width':1595,'height':1841,'quality':85}});

  Alm.file('build/$target-n.pdf').writeAsBytesSync(enpack);

  print(t.elapse);
}
