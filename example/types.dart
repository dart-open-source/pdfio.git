import 'dart:typed_data';

import 'package:pdfio/pdfio.dart';

void test(String tag,Function tester){
  print(tag);
  tester();
}

void expect(dynamic a,dynamic b){
  if(a==b) {
    print('ok');
  }else{
    print('no');
  }
}

void main() {
  test('PdfDataTypes Bool ', () {
    expect(const PBool(true).toString(), 'true');
    expect(const PBool(false).toString(), 'false');
  });

  test('PdfDataTypes Num', () {
    expect(const PNum(0).toString(), '0');
    expect(const PNum(.5).toString(), '0.50000');
    expect(const PNum(50).toString(), '50');
    expect(const PNum(50.1).toString(), '50.10000');
  });

  test('PdfDataTypes String', () {
    expect(PString.fromString('test').toString(), '(test)');
    expect(PString.fromString('Zoé').toString(), '(Zoé)');
    expect(PString.fromString('\r\n\t\b\f)()(\\').toString(), r'(\r\n\t\b\f\)\(\)\(\\)');
    expect(PString.fromString('你好').toList(), <int>[40, 254, 255, 79, 96, 89, 125, 41]);
    expect(PString.fromDate(DateTime.fromMillisecondsSinceEpoch(1583606302000)).toString(), '(D:20200307183822Z)');
    expect(PString(Uint8List.fromList(const <int>[0, 1, 2, 3, 4, 5, 6]), true).toString(), '<00010203040506>');
  });

  test('PdfDataTypes Name', () {
    expect(const PName('/Hello').toString(), '/Hello');
  });

  test('PdfDataTypes Null', () {
    expect(const PNull().toString(), 'null');
  });

  test('PdfDataTypes Indirect', () {
    expect(const PIDR(30, 4).toString(), '30 4 R');
  });

  test('PdfDataTypes Array', () {
    expect(PArray().toString(), '[]');
    expect(
      PArray(<PType>[const PNum(1), const PNum(2)]).toString(),
      '[1 2]',
    );
    expect(
      PArray(<PType>[
        const PName('/Name'),
        const PName('/Other'),
        const PBool(false),
        const PNum(2.5),
        const PNull(),
        PString.fromString('helło'),
        PArray(),
        PDict(),
      ]).toString(),
      '[/Name/Other false 2.50000 null(þÿ\x00h\x00e\x00l\x01B\x00o)[]<<>>]',
    );
  });

  test('PdfDataTypes Dict', () {
    expect(PDict().toString(), '<<>>');

    expect(
      PDict(<String, PType>{
        '/Name': const PName('/Value'),
        '/Bool': const PBool(true),
        '/Num': const PNum(42),
        '/String': PString.fromString('hello'),
        '/Null': const PNull(),
        '/Indirect': const PIDR(55, 0),
        '/Array': PArray(<PType>[]),
        '/Dict': PDict(<String, PType>{}),
      }).toString(),
      '<</Name/Value/Bool true/Num 42/String(hello)/Null null/Indirect 55 0 R/Array[]/Dict<<>>>>',
    );
  });
}
