import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:alm/alm.dart';
import 'package:byter/byter.dart';
import 'package:convert/convert.dart';
import 'package:pdfio/src/codec/codec.dart';
import 'package:utf/utf.dart';

void main() async {
  var startTime = Alm.duration;
  var file = File('/Users/alm/Documents/bbys-v1.pdf');

//  var dir = Directory('/Users/alm/Documents/');
//  dir.listSync().forEach((element) {
//  var file = File(element.path);
//  });

  if (file.existsSync()) {


//    Alm.file('build/SP1SB-77.txt').writeAsBytesSync(file.readAsBytesSync().sublist(101162943,101189401));

    try {
      var pp = PdfParser(file);
      if (pp.isPdf) {
        pp.parse();
        print(pp);
      } else {
        throw Exception('not pdf');
      }
    } catch (e) {
      print('$file->$e');
    }
  }

  print(startTime.elapse);
}

class PdfParser {
  int startXref = -1;
  int endXref = 0;

  String trailer;

  bool get isPdf => !(startXref < 0 || startXref > size);

  @override
  String toString() {
    return 'PdfParser{startXref: $startXref ,trailer:$trailer}';
  }

  Uint8List bytes;

  int get size => bytes.length;
  File file;

  PdfParser(this.file) {
    bytes = file.readAsBytesSync();

    print('PdfParser:in($size)');

    startXrefParse();
  }

  void parse() {
    if (!isPdf) throw Exception('startXref <0 || startXref>size');
    xrefParse();
//    objParse();
  }

  void startXrefParse({String key = 'startxref', int len = 50}) {
    var pos = bytes._subhex(size - len).indexOf(bytes._strhex(key)) ~/ 2;
    endXref = size - (len - pos);
    var _xt = bytes._substr(endXref);
    var re = RegExp(r'^startxref(\s+)(\d+)(\s+)%%EOF$', multiLine: true).firstMatch(_xt);
    if (re != null && re.groupCount == 3) {
      startXref = int.tryParse(re.group(2));
    }
  }

  var xrefs = <XrefId>[];

  void xrefParse() {
    xrefs.clear();
    try {
      var xrefTmp = bytes._substr(startXref, startXref + 4);
      // Alm.file('build/${Alm.basename(file.path)}.txt').writeAsBytesSync(bytes.sublist(startXref, endXref));
      if (xrefTmp == 'xref') {
        var xrefContent = bytes._substr(startXref, endXref);
        var _xt = xrefContent.split('\n');
        var pos = 1;
        while (pos < _xt.length) {
          var orr = _xt[pos].split(' ');
          if (orr.length != 2) break;
          var isNum1 = '0123456789'.codeUnits.contains(orr.first.codeUnits.first);
          var isNum2 = '0123456789'.codeUnits.contains(orr.last.codeUnits.first);
          if (!isNum1 || !isNum2) break;
          var oN =int.tryParse(orr.last);
          while (oN > 0 && pos < _xt.length) {
            pos++;
            xrefs.add(XrefId.from(_xt[pos], orr.first));
            oN--;
          }
          pos++;
        }
        if (_xt.contains('trailer')) {
          trailer = _xt.sublist(_xt.indexOf('trailer') + 1).join('\n');
        }
      } else {
        var hexXref = ObjId(bytes.sublist(startXref, endXref));
        hexXref.xref();

        var offsets=[];
        hexXref.entries.forEach((key, value) {
          offsets.add(value.first);
          xrefs.add(XrefId.from(value.join(' '), key.toString()));
        });
        offsets.sort();
//        print(hexXref.raw);
        print(offsets);
        // hexXref.entries.forEach(Alm.printMap);

        var prev=ObjId(bytes.sublist(1949029, 1949457));
        print(prev);
        print(prev.raw);
        prev.xref();
        // prev.entries.forEach(Alm.printMap);
      }
    } catch (e) {
      throw Exception('Xref-Error:$e');
    }
  }

  var objs = <ObjId>[];

  void objParse() {
    var offs = [];
    xrefs.forEach((element) {
      offs.add(element.offset);
    });
    offs.add(startXref);
    offs.sort();
    var pos = 0;
    while (pos < offs.length - 1) {
      objs.add(ObjId(bytes.sublist(offs[pos], offs[pos + 1])));
      pos++;
    }
    objs.forEach((element) {
      print(element);
    });
  }
}

class ObjId {
  Uint8List bytes;
  Uint8List content;
  Uint8List stream;

  int get size => bytes.length;
  bool hasStream = false;
  int endStream = -1;
  int startStream = -1;

  String raw;

  List<int> get destream => flateDecode(stream,width: 5,predictor: 12);

  @override
  String toString() {
    return 'ObjId{content: ${content?.length} stream:${stream?.length} }';
  }

  ObjId(this.bytes) {
    var len = min(50, size);
    var pos = bytes._subhex(size - len).indexOf(bytes._strhex('endstream'));
    if (pos > 0) {
      endStream = (size - (len - pos ~/ 2));
      hasStream = true;
      /// find start stream
      var sp = 0;
      while (sp < bytes.length) {
        var pos = bytes._subhex(sp, sp + len).indexOf(bytes._strhex('stream'));
        if (pos > 0) {
          startStream = sp + (pos ~/ 2);
          break;
        }
        sp++;
      }
      content = bytes.sublist(0, startStream);
      stream = bytes.sublist(startStream + 8, endStream - 2);
    } else {
      content = bytes;
    }
    raw = decodeUtf8(content);
  }

  var entries=<int,dynamic>{};
  void xref() {
    var idx = [5, 1, 40, 1, 43, 1, 266, 4, 271, 1, 291, 1, 295, 1, 677, 1, 681, 1, 683, 1, 693, 11];
    var w = [1, 3, 1];
    var dc = Byter(destream);
    for (int i = 0; i + 1 < idx.length; i += 2) {
      var first=idx[i];
      var n=idx[i+1];
      for (var j = first; j < first + n; ++j) {
        var type = w[0] == 0?1: dc.byte();
        var offset=bytesToInt(dc.bytes(w[1]).toList());
        var gen=bytesToInt(dc.bytes(w[2]).toList());
        entries[j]=[offset,gen,type];
      }
    }
  }
}

class XrefId {
  int offset;
  int genum;
  String flag;

  @override
  String toString() {
    return 'XrefId{offset: $offset, genum: $genum, flag: $flag, oid:$oid}';
  }

  String raw;
  String oid;

  XrefId({this.raw, this.oid}) {
    var ra = raw.trim().split(' ');
    if (ra.length != 3) throw Exception('xref !=3 ');
    offset = int.tryParse(ra.first);
    genum = int.tryParse(ra[1]);
    flag = ra.last;
  }

  static XrefId from(String raw, String oid) {
    return XrefId(raw: raw, oid: oid);
  }
}

extension _ListInt on List<int> {
  String _substr(int start, [int end]) => decodeUtf8(this.sublist(start, end));
  String _subhex(int start, [int end]) => hex.encode(this.sublist(start, end));
  String _strhex(String input) => hex.encode(input.codeUnits);
}
