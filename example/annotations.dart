// ignore_for_file: omit_local_variable_types

import 'dart:io';

import 'package:pdfio/pdfio.dart';

void main() {
  final PDocument pdf = PDocument();
  final PPage page = PPage(pdf, option: {'width':500, 'height':300});
  final PPage page1 = PPage(pdf, option: {'width':500, 'height':300});

  final PdfGraphics g = page.getGraphics();

  Annotation(
    page,
    const PAnnotText(
      rect: PRect(100, 100, 50, 50),
      content: 'Hello',
    ),
  );

  Annotation(
    page,
    const PAnnotLink(
      dest: 'target',
      rect: PRect(100, 150, 50, 50),
    ),
  );
  g.drawRect(100, 150, 50, 50);
  g.strokePath();

  Annotation(
    page,
    const PUrlLink(
      rect: PRect(100, 250, 50, 50),
      url: 'https://github.com/dart-open-source/pdf/',
    ),
  );
  g.drawRect(100, 250, 50, 50);
  g.strokePath();

  File('build/annotations.pdf').writeAsBytesSync(pdf.getBodyBytes());
}
