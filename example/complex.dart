// ignore_for_file: omit_local_variable_types

import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:pdfio/pdfio.dart';
import 'package:vector_math/vector_math_64.dart';

void main() {
  final Uint32List img = Uint32List(10 * 10);
  img.fillRange(0, img.length - 1, 0x12345678);

  final PDocument pdf = PDocument();
  final PPage page = PPage(pdf,option: {'width':500, 'height':300});

  final PdfGraphics g = page.getGraphics();

  g.saveContext();
  Matrix4 tm = Matrix4.identity();
  tm.translate(10.0, 290);
  tm.scale(1.0, -1);
  g.setTransform(tm);
  g.setColor(const PdfColor(0, 0, 0));
  g.drawShape('M37 0H9C6.24 0 4 2.24 4 5v38c0 2.76 2.24 5 5 5h28c2.76 0 5-2.24 5-5V5c0-2.76-2.24-5-5-5zM23 46c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm15-8H8V6h30v32z', stroke: false);
  g.fillPath();
  g.restoreContext();

  g.saveContext();
  tm = Matrix4.identity();
  tm.translate(200.0, 290);
  tm.scale(.1, -.1);
  g.setTransform(tm);
  g.setColor(const PdfColor(0, 0, 0));
  g.drawShape('M300,200 h-150 a150,150 0 1,0 150,-150 z M275,175 v-150 a150,150 0 0,0 -150,150 z');
  g.restoreContext();

  final PFont font1 = g.defaultFont;

  final Uint8List data = File('fonts/OpenSans-Regular.ttf').readAsBytesSync();
  final PdfTtfFont font2 = PdfTtfFont(pdf, data.buffer.asByteData());
  const String s = 'Hello World!';
  final PdfFontMetrics r = font2.stringMetrics(s);
  const double FS = 20;
  g.setColor(const PdfColor(0, 1, 1));
  g.drawRect(50.0 + r.left * FS, 30.0 + r.top * FS, r.width * FS, r.height * FS);
  g.fillPath();
  g.setColor(const PdfColor(0.3, 0.3, 0.3));
  g.drawString(font2, FS, s, 50, 30);

  g.setColor(const PdfColor(1, 0, 0));
  g.drawString(font2, 20, 'Hé (Olà)', 50, 10);
  g.drawLine(30, 30, 200, 200);
  g.strokePath();
  g.setColor(const PdfColor(1, 0, 0));
  g.drawRect(300, 150, 50, 50);
  g.fillPath();
  g.setColor(const PdfColor(0, 0.5, 0));
  //
  // final PImage image = PImage(pdf, image: img.buffer.asUint8List(), width: 10, height: 10);
  // for (double i = 10; i < 90.0; i += 5.0) {
  //   g.saveContext();
  //   final Matrix4 tm = Matrix4.identity();
  //   tm.rotateZ(i * pi / 360.0);
  //   tm.translate(300.0, -100);
  //   g.setTransform(tm);
  //   g.drawString(font1, 12, 'Hello $i', 20, 100);
  //   g.drawImage(image, 100, 100);
  //   g.restoreContext();
  // }

//  pdf.save('build/complex.pdf');
}
