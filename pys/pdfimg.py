#
# The Python Imaging Library.
# $Id$
#
# PDF (Acrobat) file handling
#
# History:
#       96-07-16 fl     Created
#       97-01-18 fl     Fixed header
#
# Copyright (c) Secret Labs AB 1997.
# Copyright (c) Fredrik Lundh 1996-97.
#
# See the README file for information on usage and redistribution.
#
import io
import os
import time

from PIL import Image,JpegImagePlugin

filename = "../res/test-1.jpg"
im = Image.open(filename)
im.encoderinfo={}

op = io.BytesIO()
Image.SAVE["JPEG"](im, op, "../build/test.pdf")

f=open("../build/test.jpg",'wb')
f.write(op.getvalue())
f.close()