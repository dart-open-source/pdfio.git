part of pdfio;

/// pdf data type
abstract class PType {
  const PType();

  void output(PStream s);

  PStream _toStream() {
    var s = PStream();
    output(s);
    return s;
  }

  @override
  String toString() => String.fromCharCodes(_toStream().output());

  @visibleForTesting
  Uint8List toList() => _toStream().output();
}

class PBool extends PType {
  const PBool(this.value);

  final bool value;

  @override
  void output(PStream s) {
    s.putString(value ? 'true' : 'false');
  }
}

class PNum extends PType {
  const PNum(this.value)
      : assert(value != null),
        assert(value != double.infinity),
        assert(value != double.nan),
        assert(value != double.negativeInfinity);

  static const int precision = 5;

  final num value;

  @override
  void output(PStream s) {
    if (value is int) {
      s.putString(value.toInt().toString());
    } else {
      s.putString(value.toStringAsFixed(precision));
    }
  }
}

class PNums extends PType {
  PNums(this.values) : assert(values != null);

  final List<num> values;

  @override
  void output(PStream s) {
    for (var n = 0; n < values.length; n++) {
      if (n > 0) {
        s.putByte(0x20);
      }
      PNum(values[n]).output(s);
    }
  }
}

class PString extends PType {
  const PString(this.value, [this.format = false]);

  factory PString.fromString(String value) {
    return PString(_string(value));
  }

  factory PString.fromDate(DateTime date) {
    return PString(_date(date));
  }

  final Uint8List value;

  final bool format;

  static Uint8List _string(String value) {
    try {
      return latin1.encode(value);
    } catch (e) {
      return Uint8List.fromList(<int>[0xfe, 0xff] + encodeUtf16be(value));
    }
  }

  static Uint8List _date(DateTime date) {
    var utcDate = date.toUtc();
    var year = utcDate.year.toString().padLeft(4, '0');
    var month = utcDate.month.toString().padLeft(2, '0');
    var day = utcDate.day.toString().padLeft(2, '0');
    var hour = utcDate.hour.toString().padLeft(2, '0');
    var minute = utcDate.minute.toString().padLeft(2, '0');
    var second = utcDate.second.toString().padLeft(2, '0');
    return _string('D:$year$month$day$hour$minute${second}Z');
  }

  /// Escape special characters
  /// \ddd Character code ddd (octal)
  void _putTextBytes(PStream s, List<int> b) {
    for (var c in b) {
      switch (c) {
        case 0x0a: // \n Line feed (LF)
          s.putByte(0x5c);
          s.putByte(0x6e);
          break;
        case 0x0d: // \r Carriage return (CR)
          s.putByte(0x5c);
          s.putByte(0x72);
          break;
        case 0x09: // \t Horizontal tab (HT)
          s.putByte(0x5c);
          s.putByte(0x74);
          break;
        case 0x08: // \b Backspace (BS)
          s.putByte(0x5c);
          s.putByte(0x62);
          break;
        case 0x0c: // \f Form feed (FF)
          s.putByte(0x5c);
          s.putByte(0x66);
          break;
        case 0x28: // \( Left parenthesis
          s.putByte(0x5c);
          s.putByte(0x28);
          break;
        case 0x29: // \) Right parenthesis
          s.putByte(0x5c);
          s.putByte(0x29);
          break;
        case 0x5c: // \\ Backslash
          s.putByte(0x5c);
          s.putByte(0x5c);
          break;
        default:
          s.putByte(c);
      }
    }
  }

  /// Returns the ASCII/Unicode code unit corresponding to the hexadecimal digit
  /// [digit].
  int _codeUnitForDigit(int digit) => digit < 10 ? digit + 0x30 : digit + 0x61 - 10;

  void _output(PStream s, Uint8List value) {
    if (format) {
      s.putByte(0x3c);
      for (var byte in value) {
        s.putByte(_codeUnitForDigit((byte & 0xF0) >> 4));
        s.putByte(_codeUnitForDigit(byte & 0x0F));
      }
      s.putByte(0x3e);
    } else {
      s.putByte(40);
      _putTextBytes(s, value);
      s.putByte(41);
    }
  }

  @override
  void output(PStream s) {
    _output(s, value);
  }
}

class PName extends PType {
  const PName(this.value);

  final String value;

  @override
  void output(PStream s) {
    assert(value[0] == '/');
    s.putString(value);
  }
}

class PNull extends PType {
  const PNull();

  @override
  void output(PStream s) {
    s.putString('null');
  }
}

class PIDR extends PType {
  const PIDR(this.ser, this.gen);

  final int ser;
  final int gen;

  @override
  void output(PStream s) {
    s.putString('$ser $gen R');
  }
}

class PArray extends PType {
  PArray([Iterable<PType> values]) {
    if (values != null) {
      this.values.addAll(values);
    }
  }

  factory PArray.fromObjects(List<PObject> objects) {
    return PArray(objects.map<PIDR>((PObject e) => e.idref()).toList());
  }

  factory PArray.fromNum(List<num> list) {
    return PArray(list.map<PNum>((num e) => PNum(e)).toList());
  }

  final List<PType> values = <PType>[];

  void add(PType v) {
    values.add(v);
  }

  @override
  void output(PStream s) {
    s.putString('[');
    if (values.isNotEmpty) {
      for (var n = 0; n < values.length; n++) {
        var val = values[n];
        if (n > 0 && !(val is PName || val is PString || val is PArray || val is PDict)) {
          s.putByte(0x20);
        }
        val.output(s);
      }
    }
    s.putString(']');
  }
}

class PDict extends PType {
  PDict([Map<String, PType> values]) {
    if (values != null) {
      this.values.addAll(values);
    }
  }

  factory PDict.fromObjectMap(Map<String, PObject> objects) {
    return PDict(
      objects.map<String, PIDR>(
        (String key, PObject value) => MapEntry<String, PIDR>(key, value.idref()),
      ),
    );
  }

  final Map<String, PType> values = <String, PType>{};

  bool get isNotEmpty => values.isNotEmpty;

  operator []=(String k, PType v) {
    values[k] = v;
  }

  @override
  void output(PStream s) {
    s.putBytes(const <int>[0x3c, 0x3c]);
    values.forEach((String k, PType v) {
      s.putString(k);
      if (v is PNum || v is PBool || v is PNull || v is PIDR) {
        s.putByte(0x20);
      }
      v.output(s);
    });
    s.putBytes(const <int>[0x3e, 0x3e]);
  }

  bool containsKey(String key) {
    return values.containsKey(key);
  }
}

class PObject {
  PObject(this.document, [String type]) {
    idr = document.nextIdr();
    if (type != null) params['/Type'] = PName(type);
    document.objects.add(this);
  }

  final PDict params = PDict();
  int idr;
  final int idg = 0;
  final PDocument document;

  void _write(PStream os) {
    _prepare();
    _writeStart(os);
    _writeContent(os);
    _writeEnd(os);
  }

  /// Prepare the object to be written to the stream
  @mustCallSuper
  void _prepare() {}

  void _writeStart(PStream os) {
    os.putString('$idr $idg obj\n');
  }

  void _writeContent(PStream os) {
    if (params.isNotEmpty) {
      params.output(os);
      os.putString('\n');
    }
  }

  void _writeEnd(PStream os) {
    os.putString('endobj\n');
  }

  PIDR idref() => PIDR(idr, idg);
}

///-------------------------------

class PObjects extends PObject {
  PObjects(PDocument document, this.array) : super(document);

  final PArray array;

  @override
  void _writeContent(PStream os) {
    super._writeContent(os);
    array.output(os);
    os.putBytes(<int>[0x0a]);
  }
}

class POStream extends PObject {
  POStream(PDocument document, {String type, this.isBinary = false}) : super(document, type);
  final PStream stream = PStream();
  final bool isBinary;
  Uint8List _data;

  @override
  void _prepare() {
    super._prepare();
    if (params.containsKey('/Filter') && _data == null) {
      _data = stream.output();
    } else {
      final Uint8List original = stream.output();
      final Uint8List newData = zlib.encode(original);
      if (newData.lengthInBytes < original.lengthInBytes) {
        params['/Filter'] = const PName('/FlateDecode');
        _data = newData;
      }
    }

    if (_data == null) {
      if (isBinary) {
        _data = Ascii85Encoder().convert(stream.output());
        params['/Filter'] = const PName('/ASCII85Decode');
      } else {
        _data = stream.output();
      }
    }
    params['/Length'] = PNum(_data.length);
  }

  @override
  void _writeContent(PStream os) {
    super._writeContent(os);
    os.putString('stream\n');
    os.putBytes(_data);
    os.putString('\nendstream\n');
  }
}

class XObject extends POStream {
  XObject(PDocument document, String subtype, {bool isBinary = false}) : super(document, type: '/XObject', isBinary: isBinary) {
    params['/Subtype'] = PName(subtype);
  }
}

class FXObject extends XObject {
  FXObject(PDocument pdfDocument) : super(pdfDocument, '/Form') {
    params['/FormType'] = const PNum(1);
    params['/BBox'] = PArray.fromNum(const <int>[0, 0, 1000, 1000]);
  }

  final Map<String, PFont> fonts = <String, PFont>{};
  final Map<String, XObject> xobjects = <String, XObject>{};

  void setMatrix(Matrix4 t) {
    final Float64List s = t.storage;
    params['/Matrix'] = PArray.fromNum(<double>[s[0], s[1], s[4], s[5], s[12], s[13]]);
  }

  @override
  void _prepare() {
    super._prepare();
    final PDict resources = PDict();
    if (fonts.isNotEmpty) resources['/Font'] = PDict.fromObjectMap(fonts);
    if (xobjects.isNotEmpty) resources['/XObject'] = PDict.fromObjectMap(xobjects);
    if (resources.isNotEmpty) params['/Resources'] = resources;
  }
}

class PImage extends XObject {
  String get name => '/Im$idr';

  final dynamic imFile;
  final PPage page;

  PImage(PDocument document, this.imFile, this.page) : super(document, '/Image', isBinary: true);

  static PImage fromImage(PDocument document, imFile, PPage page) {
    return PImage(document, imFile, page);
  }

  im.Image deimg;

  @override
  void _prepare() {
    if (imFile is im.Image) deimg = imFile;
    if (imFile is File) deimg = decodeImage(imFile.readAsBytesSync());
    if (imFile is String) deimg = decodeImage(Alm.file(imFile).readAsBytesSync());
    deimg = deimg.copyTo(
      im.Image(page.widthInPix.toInt(), page.heightInPix.toInt())..fill(page.option.get<int>('background',0xffffffff)),
      fit: page.option.get<bool>('fit', false),
    );
    params['/Width'] = PNum(deimg.width);
    params['/Height'] = PNum(deimg.height);
    params['/BitsPerComponent'] = const PNum(8);
    params['/Name'] = PName(name);
    params['/ColorSpace'] = const PName('/DeviceRGB');
    params['/Filter'] = const PName('/DCTDecode');
    stream.putBytes(deimg.saveJpeg());
    super._prepare();
  }
}

class PInfo extends PObject {
  PInfo(PDocument document) : super(document, '/Info');

  @override
  void _prepare() {
    super._prepare();
    document.option.forEach((key, value) {
      params['/${key.toUpFirst()}'] = PString.fromString(value.toString());
    });
    params['/Producer'] = PString.fromString('Dart.Pdfio');
    params['/CreationDate'] = PString.fromDate(DateTime.now());
  }
}

///-----------------------------------------------Trailers and Xref -----------------------------------------------

class PPoint {
  const PPoint(this.x, this.y);

  final double x, y;
  static const PPoint zero = PPoint(0.0, 0.0);

  @override
  String toString() => 'Point($x, $y)';
}

class PXref {
  PXref(this.id, this.offset, {this.gen = 0});

  int id;
  int offset;
  int gen = 0;

  String ref() {
    final String rs = offset.toString().padLeft(10, '0') + ' ' + gen.toString().padLeft(5, '0');
    if (gen == 65535) return rs + ' f ';
    return rs + ' n ';
  }
}

class PStream {
  static const int _grow = 65536;

  Uint8List _stream = Uint8List(_grow);

  int _offset = 0;

  void _ensureCapacity(int size) {
    if (_stream.length - _offset >= size) return;
    var newSize = _offset + size + _grow;
    var newBuffer = Uint8List(newSize);
    newBuffer.setAll(0, _stream);
    _stream = newBuffer;
  }

  void putByte(int s) {
    _ensureCapacity(1);
    _stream[_offset++] = s;
  }

  void putBytes(List<int> s) {
    _ensureCapacity(s.length);
    _stream.setAll(_offset, s);
    _offset += s.length;
  }

  void setBytes(int offset, Iterable<int> iterable) {
    _stream.setAll(offset, iterable);
  }

  void putStream(PStream s) {
    putBytes(s._stream);
  }

  int get offset => _offset;

  Uint8List output() => _stream.sublist(0, _offset);

  void putString(String s) {
    assert(() {
      for (var codeUnit in s.codeUnits) {
        if (codeUnit > 0x7f) return false;
      }
      return true;
    }());
    putBytes(s.codeUnits);
  }
}

class POutput {
  POutput(this.os) {
    os.putString('%PDF-${PDocument.version}\n');
    os.putString('%AlmPazel\n');
  }

  final PStream os;
  List<PXref> xrefs = <PXref>[];
  PObject root;
  PObject info;

  void write(PObject ob) {
    if (ob is PRoot) root = ob;
    if (ob is PInfo) info = ob;
    xrefs.add(PXref(ob.idr, os.offset));
    ob._write(os);
  }

  /// This closes the Stream, writing the xref table
  void close() {
    final xref = os.offset;
    os.putString('xref\n');
    var firstid = 0; // First id in block
    var lastid = -1; // The last id used
    final block = <PXref>[]; // xrefs in this block
    block.add(PXref(0, 0, gen: 65535));
    for (PXref xref in xrefs) {
      if (firstid == -1) firstid = xref.id;
      if (lastid > -1 && xref.id != (lastid + 1)) {
        writeblock(firstid, block);
        block.clear();
        firstid = -1;
      }
      // now add to block
      block.add(xref);
      lastid = xref.id;
    }
    if (firstid > -1) writeblock(firstid, block);
    os.putString('trailer\n');
    var params = PDict();
    params['/Size'] = PNum(xrefs.length + 1);
    params['/Root'] = root.idref();
    var id = PString(root.document.id, true);
    params['/ID'] = PArray(<PType>[id, id]);
    params['/Info'] = info.idref();
    // end the trailer object
    params.output(os);
    os.putString('\nstartxref\n$xref\n%%EOF\n');
  }

  void writeblock(int firstid, List<PXref> block) {
    os.putString('$firstid ${block.length}\n');
    for (var x in block) {
      os.putString(x.ref());
      os.putString('\n');
    }
  }
}
