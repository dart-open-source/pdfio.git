
part of pdfio;

class PdfFontDescriptor extends PObject {
  PdfFontDescriptor(
    this.ttfFont,
    this.file,
  )   : assert(ttfFont != null),
        assert(file != null),
        super(ttfFont.document, '/FontDescriptor');

  final POStream file;

  final PdfTtfFont ttfFont;

  @override
  void _prepare() {
    super._prepare();

    params['/FontName'] = PName('/' + ttfFont.fontName);
    params['/FontFile2'] = file.idref();
    params['/Flags'] = PNum(ttfFont.font.unicode ? 4 : 32);
    params['/FontBBox'] = PArray.fromNum(<int>[
      (ttfFont.font.xMin / ttfFont.font.unitsPerEm * 1000).toInt(),
      (ttfFont.font.yMin / ttfFont.font.unitsPerEm * 1000).toInt(),
      (ttfFont.font.xMax / ttfFont.font.unitsPerEm * 1000).toInt(),
      (ttfFont.font.yMax / ttfFont.font.unitsPerEm * 1000).toInt()
    ]);
    params['/Ascent'] = PNum((ttfFont.ascent * 1000).toInt());
    params['/Descent'] = PNum((ttfFont.descent * 1000).toInt());
    params['/ItalicAngle'] = const PNum(0);
    params['/CapHeight'] = const PNum(10);
    params['/StemV'] = const PNum(79);
  }
}
