
part of pdfio;

class PdfTtfFont extends PFont {
  /// Constructs a [PdfTtfFont]
  PdfTtfFont(PDocument pdfDocument, ByteData bytes, {bool protect = false})
      : font = TtfParser(bytes),
        super._create(pdfDocument, subtype: '/TrueType') {
    file = POStream(pdfDocument, isBinary: true);
    unicodeCMap = PdfUnicodeCmap(pdfDocument, protect);
    descriptor = PdfFontDescriptor(this, file);
    widthsObject = PObjects(pdfDocument, PArray());
  }

  @override
  String get subtype => font.unicode ? '/Type0' : super.subtype;

  PdfUnicodeCmap unicodeCMap;

  PdfFontDescriptor descriptor;

  POStream file;

  PObjects widthsObject;

  final TtfParser font;

  @override
  String get fontName => font.fontName;

  @override
  double get ascent => font.ascent.toDouble() / font.unitsPerEm;

  @override
  double get descent => font.descent.toDouble() / font.unitsPerEm;

  @override
  PdfFontMetrics glyphMetrics(int charCode) {
    var g = font.charToGlyphIndexMap[charCode];
    if (g == null) {
      return PdfFontMetrics.zero;
    }
    if (PdfArabic._isArabicDiacriticValue(charCode)) {
      var metric = font.glyphInfoMap[g] ?? PdfFontMetrics.zero;
      return metric.copyWith(advanceWidth: 0);
    }
    return font.glyphInfoMap[g] ?? PdfFontMetrics.zero;
  }

  void _buildTrueType(PDict params) {
    int charMin;
    int charMax;

    file.stream.putBytes(font.bytes.buffer.asUint8List());
    file.params['/Length1'] = PNum(font.bytes.lengthInBytes);

    params['/BaseFont'] = PName('/' + fontName);
    params['/FontDescriptor'] = descriptor.idref();
    charMin = 32;
    charMax = 255;
    for (var i = charMin; i <= charMax; i++) {
      widthsObject.array
          .add(PNum((glyphMetrics(i).advanceWidth * 1000.0).toInt()));
    }
    params['/FirstChar'] = PNum(charMin);
    params['/LastChar'] = PNum(charMax);
    params['/Widths'] = widthsObject.idref();
  }

  void _buildType0(PDict params) {
    int charMin;
    int charMax;

    var ttfWriter = TtfWriter(font);
    var data = ttfWriter.withChars(unicodeCMap.cmap);
    file.stream.putBytes(data);
    file.params['/Length1'] = PNum(data.length);

    var descendantFont = PDict(<String, PType>{
      '/Type': const PName('/Font'),
      '/BaseFont': PName('/' + fontName),
      '/FontFile2': file.idref(),
      '/FontDescriptor': descriptor.idref(),
      '/W': PArray(<PType>[
        const PNum(0),
        widthsObject.idref(),
      ]),
      '/CIDToGIDMap': const PName('/Identity'),
      '/DW': const PNum(1000),
      '/Subtype': const PName('/CIDFontType2'),
      '/CIDSystemInfo': PDict(<String, PType>{
        '/Supplement': const PNum(0),
        '/Registry': PString.fromString('Adobe'),
        '/Ordering': PString.fromString('Identity-H'),
      })
    });

    params['/BaseFont'] = PName('/' + fontName);
    params['/Encoding'] = const PName('/Identity-H');
    params['/DescendantFonts'] = PArray(<PType>[descendantFont]);
    params['/ToUnicode'] = unicodeCMap.idref();

    charMin = 0;
    charMax = unicodeCMap.cmap.length - 1;
    for (var i = charMin; i <= charMax; i++) {
      widthsObject.array.add(PNum((glyphMetrics(unicodeCMap.cmap[i]).advanceWidth * 1000.0).toInt()));
    }
  }

  @override
  void _prepare() {
    super._prepare();

    if (font.unicode) {
      _buildType0(params);
    } else {
      _buildTrueType(params);
    }
  }

  @override
  void putText(PStream stream, String text) {
    if (!font.unicode) {
      super.putText(stream, text);
    }
    var runes = text.runes;
    stream.putByte(0x3c);
    for (var rune in runes) {
      var char = unicodeCMap.cmap.indexOf(rune);
      if (char == -1) {
        char = unicodeCMap.cmap.length;
        unicodeCMap.cmap.add(rune);
      }
      stream.putBytes(latin1.encode(char.toRadixString(16).padLeft(4, '0')));
    }
    stream.putByte(0x3e);
  }

  @override
  PdfFontMetrics stringMetrics(String s, {double letterSpacing = 0}) {
    if (s.isEmpty || !font.unicode) {
      return super.stringMetrics(s, letterSpacing: letterSpacing);
    }
    var runes = s.runes;
    var bytes = <int>[];
    runes.forEach(bytes.add);
    var metrics = bytes.map(glyphMetrics);
    return PdfFontMetrics.append(metrics, letterSpacing: letterSpacing);
  }
}
