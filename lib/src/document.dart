part of pdfio;

class PDocument {
  static String version = '1.5';
  Uint8List id;

  RandomAccessFile stream;

  PDocument({this.title='none'}) {
    option['title']=title;
    id=Uint8List.fromList(Alm.times.codeUnits);
    root = PRoot(this);
    info = PInfo(this);
    page=PPages(this);
  }
  int _idr =1;
  final List<PObject> objects = <PObject>[];
  final List<PFont> fonts = <PFont>[];
  final List<PPage> pages = <PPage>[];

  PRoot root;
  PInfo info;
  PPages page;

  Outline _outline;
  PdfGraphicStates _graphicStates;

  String title ='none';
  Map<String,dynamic> option ={};

  int nextIdr() => _idr++;

  PPage getPage(int page) =>pages[page];
  PPage newPage({option}) =>PPage(this,option:option);

  Outline get outline {
    if (_outline == null) {
      _outline = Outline(this);
      root.outlines = _outline;
    }
    return _outline;
  }

  PdfGraphicStates get graphicStates {
    _graphicStates ??= PdfGraphicStates(this);
    return _graphicStates;
  }

  bool get hasGraphic => _graphicStates != null;
  Uint8List bytes;
  Uint8List getBodyBytes() {
    if(bytes==null){
      var os = PStream();
      var pos = POutput(os);
      objects.forEach(pos.write);
      pos.close();
      bytes=os.output();
    }
    return bytes;
  }

  void save(String s) {}
}
