

part of pdfio;

@immutable
class PRect {
  const PRect(this.x, this.y, this.width, this.height);

  factory PRect.fromLTRB(
      double left, double top, double right, double bottom) {
    return PRect(left, top, right - left, bottom - top);
  }

  final double x, y, width, height;

  static const PRect zero = PRect(0, 0, 0, 0);

  double get left => x;
  double get bottom => y;
  double get right => x + width;
  double get top => y + height;

  double get hCenter => x + width / 2;
  double get vCenter => y + height / 2;

  @override
  String toString() => 'PRect($x, $y, $width, $height)';

  PRect operator *(double factor) {
    return PRect(x * factor, y * factor, width * factor, height * factor);
  }

  PPoint get offset => PPoint(x, y);
  PPoint get size => PPoint(width, height);

  PPoint get topLeft => PPoint(x, y);
  PPoint get topRight => PPoint(right, y);
  PPoint get bottomLeft => PPoint(x, top);
  PPoint get bottomRight => PPoint(right, top);
}
