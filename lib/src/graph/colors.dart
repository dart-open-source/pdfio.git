// ignore_for_file: omit_local_variable_types

part of pdfio;

class PdfColors {
  PdfColors._();

  static const PdfColor red =PdfColor.fromInt(0xfff44336);
  static const PdfColor pink = PdfColor.fromInt(0xffe91e63);
  static const PdfColor purple = PdfColor.fromInt(0xff9c27b0);
  static const PdfColor deepPurple = PdfColor.fromInt(0xff673ab7);
  static const PdfColor indigo = PdfColor.fromInt(0xff3f51b5);
  static const PdfColor blue = PdfColor.fromInt(0xff2196f3);
  static const PdfColor lightBlue =PdfColor.fromInt(0xff03a9f4);
  static const PdfColor cyan = PdfColor.fromInt(0xff00bcd4);
  static const PdfColor teal = PdfColor.fromInt(0xff009688);
  static const PdfColor green = PdfColor.fromInt(0xff4caf50);
  static const PdfColor lightGreen = PdfColor.fromInt(0xff8bc34a);
  static const PdfColor lime = PdfColor.fromInt(0xffcddc39);
  static const PdfColor yellow =  PdfColor.fromInt(0xffffeb3b);
  static const PdfColor amber = PdfColor.fromInt(0xffffc107);
  static const PdfColor orange = PdfColor.fromInt(0xffff9800);
  static const PdfColor deepOrange = PdfColor.fromInt(0xffff5722);
  static const PdfColor brown =  PdfColor.fromInt(0xff795548);
  static const PdfColor grey =PdfColor.fromInt(0xff9e9e9e);
  static const PdfColor blueGrey = PdfColor.fromInt(0xff607d8b);
  static const PdfColor white = PdfColor.fromInt(0xffffffff);
  static const PdfColor black = PdfColor.fromInt(0xff000000);

  static PdfColor getColor(int index) {
    final double hue = index * 137.508;
    final PdfColor color = PdfColorHsv(hue % 360, 1, 1);
    if ((index / 3) % 2 == 0) {
      return PdfColor.fromRYB(color.red, color.green, color.blue);
    }
    return color;
  }
}
