part of pdfio;

enum PdfOutlineMode { fitpage, fitrect }

class Outline extends PObject {
  Outline(PDocument pdfDocument, {this.title, this.dest, this.rect}) : super(pdfDocument, '/Outlines');

  List<Outline> outlines = <Outline>[];

  Outline parent;

  final String title;

  PPage dest;

  final PRect rect;

  PdfOutlineMode destMode = PdfOutlineMode.fitpage;

  Outline add({String title, PPage dest, PRect rect}) {
    final Outline outline = Outline(document, title: title, dest: dest, rect: rect);
    outline.parent = this;
    return outline;
  }

  @override
  void _prepare() {
    super._prepare();
    if (parent != null) {
      params['/Title'] = PString.fromString(title);
      final PArray dests = PArray();
      dests.add(dest.idref());

      if (destMode == PdfOutlineMode.fitpage) {
        dests.add(const PName('/Fit'));
      } else {
        dests.add(const PName('/FitR'));
        dests.add(PNum(rect.left));
        dests.add(PNum(rect.bottom));
        dests.add(PNum(rect.right));
        dests.add(PNum(rect.top));
      }
      params['/Parent'] = parent.idref();
      params['/Dest'] = dests;

      final int c = descendants();
      if (c > 0) params['/Count'] = PNum(-c);

      final int index = parent.getIndex(this);
      if (index > 0) params['/Prev'] = parent.getNode(index - 1).idref();

      if (index < parent.getLast()) params['/Next'] = parent.getNode(index + 1).idref();
    } else {
      params['/Count'] = PNum(outlines.length);
    }

    // These only valid if we have children
    if (outlines.isNotEmpty) {
      params['/First'] = outlines[0].idref();
      params['/Last'] = outlines[outlines.length - 1].idref();
    }
  }

  int getIndex(Outline outline) => outlines.indexOf(outline);

  int getLast() => outlines.length - 1;

  Outline getNode(int i) => outlines[i];

  int descendants() {
    int c = outlines.length; // initially the number of kids
    for (Outline o in outlines) {
      c += o.descendants();
    }
    return c;
  }
}
