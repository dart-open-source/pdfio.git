

// ignore_for_file: omit_local_variable_types

part of pdfio;

enum PdfShadingType { axial, radial }

class PdfShading extends PObject {
  PdfShading(
    PDocument pdfDocument, {
    @required this.shadingType,
    @required this.function,
    @required this.start,
    @required this.end,
    this.radius0,
    this.radius1,
    this.boundingBox,
    this.extendStart = false,
    this.extendEnd = false,
  })  : assert(shadingType != null),
        assert(function != null),
        assert(start != null),
        assert(end != null),
        assert(extendStart != null),
        assert(extendEnd != null),
        super(pdfDocument);

  /// Name of the Shading object
  String get name => '/S$idr';

  final PdfShadingType shadingType;

  final PdfBaseFunction function;

  final PPoint start;

  final PPoint end;

  final PRect boundingBox;

  final bool extendStart;

  final bool extendEnd;

  final double radius0;

  final double radius1;

  @override
  void _prepare() {
    super._prepare();

    params['/ShadingType'] = PNum(shadingType.index + 2);
    if (boundingBox != null) {
      params['/BBox'] = PArray.fromNum(<double>[
        boundingBox.left,
        boundingBox.bottom,
        boundingBox.right,
        boundingBox.top,
      ]);
    }
    params['/AntiAlias'] = const PBool(true);
    params['/ColorSpace'] = const PName('/DeviceRGB');

    if (shadingType == PdfShadingType.axial) {
      params['/Coords'] =PArray.fromNum(<double>[start.x, start.y, end.x, end.y]);
    } else if (shadingType == PdfShadingType.radial) {
      assert(radius0 != null);
      assert(radius1 != null);
      params['/Coords'] = PArray.fromNum(<double>[start.x, start.y, radius0, end.x, end.y, radius1]);
    }
    // params['/Domain'] = PdfArray.fromNum(<num>[0, 1]);
    if (extendStart || extendEnd) {
      params['/Extend'] =
          PArray(<PBool>[PBool(extendStart), PBool(extendEnd)]);
    }
    params['/Function'] = function.idref();
  }
}
