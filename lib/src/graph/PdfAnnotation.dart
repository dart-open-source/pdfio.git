part of pdfio;

class Annotation extends PObject {
  Annotation(this.pdfPage, this.annot)
      : assert(annot != null),
        super(pdfPage.document, '/Annot') {
    pdfPage.annotations.add(this);
  }

  /// The annotation content
  final PdfAnnotBase annot;

  /// The page where the annotation will display
  final PPage pdfPage;

  /// Output the annotation
  ///
  /// @param os OutputStream to send the object to
  @override
  void _prepare() {
    super._prepare();
    annot.build(pdfPage, this, params);
  }
}

enum PdfAnnotFlags { invisible, hidden, print, noZoom, noRotate, noView, readOnly, locked, toggleNoView, lockedContent }

abstract class PdfAnnotBase {
  const PdfAnnotBase({
    @required this.subtype,
    @required this.rect,
    this.border,
    this.content,
    this.name,
    this.flags,
    this.date,
    this.color,
  })  : assert(subtype != null),
        assert(rect != null);

  /// The subtype of the outline, ie text, note, etc
  final String subtype;

  final PRect rect;

  /// the border for this annotation
  final PdfBorder border;

  /// The text of a text annotation
  final String content;

  /// The internal name for a link
  final String name;

  /// Flags specifying various characteristics of the annotation
  final Set<PdfAnnotFlags> flags;

  /// Last modification date
  final DateTime date;

  /// Color
  final PdfColor color;

  int get flagValue => flags?.map<int>((PdfAnnotFlags e) => 1 >> e.index)?.reduce((int a, int b) => a | b);

  @protected
  @mustCallSuper
  void build(PPage page, PObject object, PDict params) {
    params['/Subtype'] = PName(subtype);
    params['/Rect'] = PArray.fromNum(<double>[rect.left, rect.bottom, rect.right, rect.top]);

    params['/P'] = page.idref();

    // handle the border
    if (border == null) {
      params['/Border'] = PArray.fromNum(const <int>[0, 0, 0]);
    } else {
      params['/BS'] = border.idref();
    }

    if (content != null) {
      params['/Contents'] = PString.fromString(content);
    }

    if (name != null) {
      params['/NM'] = PString.fromString( name);
    }

    if (flags != null) {
      params['/F'] = PNum(flagValue);
    }

    if (date != null) {
      params['/M'] = PString.fromDate(date);
    }

    if (color != null) {
      if (color is PdfColorCmyk) {
        final PdfColorCmyk k = color;
        params['/C'] = PArray.fromNum(<double>[k.cyan, k.magenta, k.yellow, k.black]);
      } else {
        params['/C'] = PArray.fromNum(<double>[color.red, color.green, color.blue]);
      }
    }
  }
}

class PAnnotText extends PdfAnnotBase {
  /// Create a text annotation
  const PAnnotText({
    @required PRect rect,
    @required String content,
    PdfBorder border,
    String name,
    Set<PdfAnnotFlags> flags,
    DateTime date,
    PdfColor color,
  }) : super(
          subtype: '/Text',
          rect: rect,
          border: border,
          content: content,
          name: name,
          flags: flags,
          date: date,
          color: color,
        );
}

class PAnnotLink extends PdfAnnotBase {
  /// Create a named link annotation
  const PAnnotLink({
    @required PRect rect,
    @required this.dest,
    PdfBorder border,
    Set<PdfAnnotFlags> flags,
    DateTime date,
    PdfColor color,
  }) : super(
          subtype: '/Link',
          rect: rect,
          border: border,
          flags: flags,
          date: date,
          color: color,
        );

  final String dest;

  @override
  void build(PPage page, PObject object, PDict params) {
    super.build(page, object, params);
    params['/A'] = PDict(
      <String, PType>{
        '/S': const PName('/GoTo'),
        '/D': PString.fromString(dest),
      },
    );
  }
}

class PUrlLink extends PdfAnnotBase {
  /// Create an url link annotation
  const PUrlLink({
    @required PRect rect,
    @required this.url,
    PdfBorder border,
    Set<PdfAnnotFlags> flags,
    DateTime date,
    PdfColor color,
  }) : super(
          subtype: '/Link',
          rect: rect,
          border: border,
          flags: flags,
          date: date,
          color: color,
        );

  final String url;

  @override
  void build(PPage page, PObject object, PDict params) {
    super.build(page, object, params);
    params['/A'] = PDict(
      <String, PType>{
        '/S': const PName('/URI'),
        '/URI': PString.fromString(url),
      },
    );
  }
}

enum PdfAnnotHighlighting { none, invert, outline, push, toggle }

abstract class PdfAnnotWidget extends PdfAnnotBase {
  /// Create an url link annotation
  const PdfAnnotWidget(
    PRect rect,
    this.fieldType, {
    this.fieldName,
    PdfBorder border,
    Set<PdfAnnotFlags> flags,
    DateTime date,
    PdfColor color,
    this.highlighting,
  }) : super(
          subtype: '/Widget',
          rect: rect,
          border: border,
          flags: flags,
          date: date,
          color: color,
        );

  final String fieldType;

  final String fieldName;

  final PdfAnnotHighlighting highlighting;

  @override
  void build(PPage page, PObject object, PDict params) {
    super.build(page, object, params);
    params['/FT'] = PName(fieldType);
    if (fieldName != null) {
      params['/T'] = PString.fromString(fieldName);
    }
  }
}

