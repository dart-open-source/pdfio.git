// ignore_for_file: omit_local_variable_types

part of pdfio;

abstract class PdfBaseFunction extends PObject {
  PdfBaseFunction(PDocument pdfDocument) : super(pdfDocument);
}

class PdfFunction extends POStream implements PdfBaseFunction {
  PdfFunction(
    PDocument pdfDocument, {
    this.colors,
  }) : super(pdfDocument);

  final List<PdfColor> colors;

  @override
  void _prepare() {
    for (final PdfColor color in colors) {
      stream.putBytes(<int>[
        (color.red * 255.0).round() & 0xff,
        (color.green * 255.0).round() & 0xff,
        (color.blue * 255.0).round() & 0xff,
      ]);
    }

    super._prepare();

    params['/FunctionType'] = const PNum(0);
    params['/BitsPerSample'] = const PNum(8);
    params['/Order'] = const PNum(3);
    params['/Domain'] = PArray.fromNum(const <num>[0, 1]);
    params['/Range'] = PArray.fromNum(const <num>[0, 1, 0, 1, 0, 1]);
    params['/Size'] = PArray.fromNum(<int>[colors.length]);
  }
}

class PdfStitchingFunction extends PdfBaseFunction {
  PdfStitchingFunction(
    PDocument pdfDocument, {
    @required this.functions,
    @required this.bounds,
    this.domainStart = 0,
    this.domainEnd = 1,
  })  : assert(functions != null),
        assert(bounds != null),
        super(pdfDocument);

  final List<PdfFunction> functions;

  final List<double> bounds;

  final double domainStart;

  final double domainEnd;

  @override
  void _prepare() {
    super._prepare();

    params['/FunctionType'] = const PNum(3);
    params['/Functions'] = PArray.fromObjects(functions);
    params['/Order'] = const PNum(3);
    params['/Domain'] = PArray.fromNum(<num>[domainStart, domainEnd]);
    params['/Bounds'] = PArray.fromNum(bounds);
    params['/Encode'] = PArray.fromNum(List<int>.generate(functions.length * 2, (int i) => i % 2));
  }
}
