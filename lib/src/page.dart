// ignore_for_file: omit_local_variable_types

part of pdfio;

class PPages extends PObject {
  PDocument document;

  PPages(this.document) : super(document, '/Pages');

  @override
  void _prepare() {
    super._prepare();
    params['/Kids'] = PArray.fromObjects(document.pages);
    params['/Count'] = PNum(document.pages.length);
  }
}

class PRoot extends PObject {
  PDocument document;

  PRoot(this.document) : super(document, '/Catalog');
  Outline outlines;

  @override
  void _prepare() {
    super._prepare();
    params['/Version'] = PName('/${PDocument.version}');
    params['/Pages'] = document.page.idref();
    if (outlines != null && outlines.outlines.isNotEmpty) {
      params['/Outlines'] = outlines.idref();
    }
  }
}

class PPage extends PObject {
  static const double point = 1.0;
  static const double inch = 72.0;
  static const double cm = inch / 2.54;
  static const double mm = inch / 25.4;

  PPage(PDocument document, {option}) : super(document, '/Page') {
    document.pages.add(this);
    this.option.mergeInto(option);
  }

  Map<String, dynamic> option = {};

  double get width => option.get<double>('width', 21.1 * cm);

  double get height => option.get<double>('height',  29.7 * cm);

  double get dpi => option.get<double>('dpi', 120.0);

  bool get isLandscape => width > height;

  double get widthInPix => (width / inch) * dpi;

  double get heightInPix => (height / inch) * dpi;

  double get ratio => width / height;

  List<POStream> contents = <POStream>[];

  PObject thumbnail;

  bool isolatedTransparency = false;

  bool knockoutTransparency = false;

  List<Annotation> annotations = <Annotation>[];

  final Map<String, PFont> fonts = <String, PFont>{};
  final Map<String, PdfShading> shading = <String, PdfShading>{};

  final Map<String, XObject> xObjects = <String, XObject>{};

  PdfGraphics getGraphics() {
    final POStream stream = POStream(document);
    final PdfGraphics g = PdfGraphics(this, stream.stream);
    contents.add(stream);
    return g;
  }

  /// @param os OutputStream to send the object to
  @override
  void _prepare() {
    super._prepare();

    params['/Parent'] = document.page.idref();
    params['/MediaBox'] = PArray.fromNum(<double>[0, 0, width, height]);

    // the /Contents pages object
    if (contents.isNotEmpty) {
      if (contents.length == 1) {
        params['/Contents'] = contents.first.idref();
      } else {
        params['/Contents'] = PArray.fromObjects(contents);
      }
    }

    final PDict resources = PDict();

    resources['/ProcSet'] = PArray([PName('/PDF'), PName('/Text'), PName('/ImageB'), PName('/ImageC')]);

    if (fonts.isNotEmpty) resources['/Font'] = PDict.fromObjectMap(fonts);
    if (shading.isNotEmpty) resources['/Shading'] = PDict.fromObjectMap(shading);
    if (xObjects.isNotEmpty) resources['/XObject'] = PDict.fromObjectMap(xObjects);

    if (document.hasGraphic) {
      // Declare Transparency Group settings
      params['/Group'] = PDict(<String, PType>{
        '/Type': const PName('/Group'),
        '/S': const PName('/Transparency'),
        '/CS': const PName('/DeviceRGB'),
        '/I': PBool(isolatedTransparency),
        '/K': PBool(knockoutTransparency),
      });
      resources['/ExtGState'] = document.graphicStates.idref();
    }

    params['/Resources'] = resources;

    if (thumbnail != null) params['/Thumb'] = thumbnail.idref();
    if (annotations.isNotEmpty) params['/Annots'] = PArray.fromObjects(annotations);
  }

  void fillImage(imFile) {
    var img = PImage.fromImage(document, imFile, this);
    var canvas = getGraphics();
    var dst = PRect(0.0, 0.0, width, height);
    var src = PRect(0.0, 0.0, widthInPix, heightInPix);
    var fw = dst.width / src.width;
    var fh = dst.height / src.height;
    canvas.saveContext();
    canvas.drawRect(dst.x, dst.y, dst.width, dst.height);
    canvas.clipPath();
    canvas.drawImage(img, dst.x - src.x * fw, dst.y - src.y * fh, widthInPix * fw, heightInPix * fh);
    canvas.restoreContext();
  }
}
