import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:byter/byter.dart';


List<int> flateDecode(List<int> origin,{int predictor=1,int width=1,int bpc=8,int colors=1}) {
  var input=zlib.decode(origin);
  if (predictor < 10 && predictor != 2) return input;
  var dataStream = Byter(input);
  var fout = <int>[];
  int bytesPerPixel = colors * bpc ~/ 8;
  int bytesPerRow = (colors * width * bpc + 7) ~/ 8;
  if (predictor == 2) {
    if (bpc == 8) {
      int numRows = input.length ~/ bytesPerRow;
      for (int row = 0; row < numRows; row++) {
        int rowStart = row * bytesPerRow;
        for (int col = 0 + bytesPerPixel; col < bytesPerRow; col++) {
          fout[rowStart + col] = (input[rowStart + col] + input[rowStart + col - bytesPerPixel]);
        }
      }
    }
  }else{
    var curr=Uint8List(bytesPerRow);
    var prior=Uint8List(bytesPerRow);

    while (true) {
      int filter = dataStream.byte();
      if (filter==null||filter < 0) break;
      curr=dataStream.bytes(bytesPerRow).toUint8List();
      switch (filter) {
        case 0: //PNG_FILTER_NONE
          break;
        case 1: //PNG_FILTER_SUB
          for (int i = bytesPerPixel; i < bytesPerRow; i++) {
            curr[i] += curr[i - bytesPerPixel];
          }
          break;
        case 2: //PNG_FILTER_UP
          for (int i = 0; i < bytesPerRow; i++) {
            curr[i] += prior[i];
          }
          break;
        case 3: //PNG_FILTER_AVERAGE
          for (int i = 0; i < bytesPerPixel; i++) {
            curr[i] += prior[i] ~/ 2;
          }
          for (int i = bytesPerPixel; i < bytesPerRow; i++) {
            curr[i] += ((curr[i - bytesPerPixel] & 0xff) + (prior[i] & 0xff))~/2;
          }
          break;
        case 4: //PNG_FILTER_PAETH
          for (int i = 0; i < bytesPerPixel; i++) {
            curr[i] += prior[i];
          }
          for (int i = bytesPerPixel; i < bytesPerRow; i++) {
            int a = curr[i - bytesPerPixel] & 0xff;
            int b = prior[i] & 0xff;
            int c = prior[i - bytesPerPixel] & 0xff;

            var abs=(int x)=>x < 0 ? -x : x;

            int p = a + b - c;
            int pa = abs(p - a);
            int pb = abs(p - b);
            int pc = abs(p - c);

            int ret;

            if (pa <= pb && pa <= pc) {
              ret = a;
            } else if (pb <= pc) {
              ret = b;
            } else {
              ret = c;
            }
            curr[i] += Uint8List.fromList([ret]).first;
          }
          break;
        default:
        // Error -- unknown filter type
          throw Exception("png.filter.unknown");
      }
      fout.addAll(curr);
      // Swap curr and prior
      var tmp = Uint8List.fromList(prior);
      prior = curr;
      curr = tmp;
    }
  }
  return Uint8List.fromList(fout);
}

int bytesToInt(List<int> bytes) {
  var value = 0;
  for (var byte in bytes) {
    value = (value << 8) + byte;
  }
  return value;
}

List<List> chunk(List letters, int size) {
  var chunks = <List>[];
  for (var i = 0; i < letters.length; i += size) {
    chunks.add(letters.sublist(i, i + size > letters.length ? letters.length : i + size));
  }
  return chunks;
}

class Ascii85Encoder extends Converter<Uint8List, Uint8List> {
  @override
  Uint8List convert(Uint8List input) {
    final Uint8List output = Uint8List(_maxEncodedLen(input.length) + 2);

    int outputOffset = 0;
    int inputOffset = 0;

    while (inputOffset < input.length) {
      output[outputOffset + 0] = 0;
      output[outputOffset + 1] = 0;
      output[outputOffset + 2] = 0;
      output[outputOffset + 3] = 0;
      output[outputOffset + 4] = 0;

      // Unpack 4 bytes into int to repack into base 85 5-byte.
      int value = 0;

      switch (input.length - inputOffset) {
        case 3:
          value |= input[inputOffset + 0] << 24;
          value |= input[inputOffset + 1] << 16;
          value |= input[inputOffset + 2] << 8;
          break;
        case 2:
          value |= input[inputOffset + 0] << 24;
          value |= input[inputOffset + 1] << 16;
          break;
        case 1:
          value |= input[inputOffset + 0] << 24;
          break;
        default:
          value |= input[inputOffset + 0] << 24;
          value |= input[inputOffset + 1] << 16;
          value |= input[inputOffset + 2] << 8;
          value |= input[inputOffset + 3];
      }

      // Special case: zero (!!!!!) shortens to z.
      if (value == 0 && input.length - inputOffset >= 4) {
        output[outputOffset] = 122;
        outputOffset++;
        inputOffset += 4;
        continue;
      }

      // Otherwise, 5 base 85 digits starting at !.
      for (int i = 4; i >= 0; i--) {
        output[outputOffset + i] = 33 + value % 85;
        value ~/= 85;
      }

      if (input.length - inputOffset < 4) {
        // If input was short, discard the low destination bytes.
        outputOffset += input.length - inputOffset + 1;
        break;
      }

      inputOffset += 4;
      outputOffset += 5;
    }

    output[outputOffset] = 0x7e;
    output[outputOffset + 1] = 0x3e;

    return output.sublist(0, outputOffset + 2);
  }

  int _maxEncodedLen(int length) => (length + 3) ~/ 4 * 5;
}
