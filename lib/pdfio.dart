library pdfio;

import 'package:process_run/process_run.dart';

import 'src/codec/codec.dart';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:math' as math;
import 'dart:typed_data';

import 'package:alm/alm.dart';
import 'package:image/image.dart' as im;
import 'package:image/image.dart';
import 'package:meta/meta.dart';
import 'package:path_parsing/path_parsing.dart';
import 'package:utf/utf.dart';
import 'package:vector_math/vector_math_64.dart';

part 'src/graph/PdfAnnotation.dart';

part 'src/font/PdfArabic.dart';

part 'src/graph/PdfBorder.dart';

part 'src/graph/PdfColor.dart';

part 'src/graph/colors.dart';

part 'src/document.dart';

part 'src/font/font.dart';

part 'src/font/font_descriptor.dart';

part 'src/font/font_metrics.dart';

part 'src/font/ttf_parser.dart';

part 'src/font/ttf_writer.dart';

part 'src/font/ttffont.dart';

part 'src/font/type1_font.dart';

part 'src/font/type1_fonts.dart';

part 'src/font/unicode_cmap.dart';

part 'src/graph/function.dart';

part 'src/graph/graphic_state.dart';

part 'src/graph/graphics.dart';

part 'src/graph/PPoint.dart';

part 'src/graph/rect.dart';

part 'src/graph/shading.dart';

part 'src/graph/outline.dart';

part 'src/object.dart';

part 'src/page.dart';

Future<String> fromImages(List<dynamic> images, String output, [Map option]) async {
  var doc = PDocument();
  doc.option.mergeInto({'creator': 'DartPdfio:(Alm)','title':'fromImages'});
  doc.option.mergeInto(option.get('info'));
  option.remove('info');
  for (var deImg in images) {
    doc.newPage(option: option).fillImage(deImg);
  }
  Alm.file(output).writeAsBytesSync(doc.getBodyBytes());
  return output;
}

Future<Map> info(String path) => cmdLine(path, cmd: 'pdfinfo');

Future<Map> fonts(String path) => cmdLine(path, cmd: 'pdffonts');

Future<Map> toImages(String path, String to, {int dpi = 150}) => cmdLine('-jpeg -r $dpi -cropbox $path $to', cmd: 'pdftocairo');

Future<Map> repair(String path, String to) => cmdLine('-pdf $path $to', cmd: 'pdftocairo');
Future<Map> toSplit(String path, String to, {int f = 1,int l = 1}) => cmdLine('-f $f -l $l $path $to', cmd: 'pdfseparate');

Future<String> toText(String path, {int f = 1,int l = 1}) async {
  var tmp = Alm.file('build/tmp/${Alm.objectId}', auto: true);
  await cmdLine('-f $l -l $l $path ${tmp.path}', cmd: 'pdftotext');
  var str = tmp.tryString();
  tmp.tryDelete();
  return str??'';
}

Future<Map> cmdLine(String cmds, {String cmd = 'pdfinfo'}) async {
  var map = <String, dynamic>{};
  try {
    var _res = await runExecutableArguments(cmd, cmds.split(' '), stderrEncoding: null, stdoutEncoding: null);
    var lines = List.from(_res.stdout).tryUtf8().split('\n');
    var error = List.from(_res.stderr).tryUtf8().split('\n');
    for (var element in error) {
      var kv = element.split(': ');
      map[kv.first.trim().replaces(' ')] = kv.last.trim();
    }
    if (cmd.contains('pdfinfo')) {
      for (var element in lines) {
        var kv = element.split(': ');
        map[kv.first.trim().replaces(' ')] = kv.last.trim();
      }
    }
    if (cmd.contains('pdffonts')) {
      var ffTag = '------------------------------------';
      for (var element in lines) {
        var k = element.tryCut(ffTag.length).trim().replaces(' ');
        if (k == ffTag || k == 'name' || k.isEmptyOrNull) continue;
        var v = element.trySub(k.length);
        map[k] = element.trySub(k.length).trim();
      }
    }
    // ignore: empty_catches
  } catch (e) {
    print(e);
  }
  return map;
}

Future<String> render(String fileIn,String fileOut,{dpi = 200,option}) async {
  var resInfo = await info(fileIn);
  if (resInfo.has('Pages')) {
    var dir = Alm.dir(fileIn + '.imgs/', auto: true);
    try{
      await toImages(fileIn, dir.path, dpi: dpi);
      var files = dir.listSync().whereType<File>().map((e) => e.path).toList()..sort();
      if (files.isNotEmpty) {
        await fromImages(files, fileOut, <String,dynamic>{
          'info': {'creator': 'DartPdfio:(Alm)','title':'rendered'},
          'dpi': dpi,
          'fit': true
        }.mergeInto(option));
      }
    }catch(e){}
    dir.tryDelete(recursive: true);
  }else{
    throw Exception('Pdf not have pages?!# ${resInfo}');
  }
  return fileOut;
}
