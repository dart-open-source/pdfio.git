import 'dart:io';
import 'package:alm/alm.dart';
import 'package:convert/convert.dart';
import 'package:image/image.dart';
import 'package:pdfio/src/codec/codec.dart';

///
///  Author: almpazel@gmail.com
///

@deprecated
void printPretty(dynamic json) => json.printPretty();

class Pack {
  static const int ByteSlash = 47; //='/'
  static var version = 'PDF-1.5';

  static const KeyIndirect = '/Indirect';
  static const KeyStream = '/Stream';
  static const KeyRoot = '/';
  static const KeyType = '/Type';
  static const KeyLength = '/Length';

  final List<int> bytes;

  var output = <int>[];
  var dicts = <Map>[];
  var links = <String, Map>{};
  Map trailer = {};
  Map option = {};
  Map info = {
    '/Producer':'(Alm*PDF (https://github.com/almpazel))',
    '/Creator':'(BBYS)',
  };

  int pos = 0;

  int get byte => bytes[pos];

  bool get isLooping => pos < size;
  String lastIndirect;

  bool get isNum => '0123456789.'.codeUnits.contains(byte);

  bool get isDelimiter => '<{[(/)]}>'.codeUnits.contains(byte);

  bool get isSpaceAll =>  [0, 9, 10, 12, 13, 32].contains(byte);

  bool get isSpaceBr => [0, 9, 10, 12, 13].contains(byte);

  Pack(this.bytes);

  int get size => bytes.length;



  ///---------------------------------------------------------------------------------------------------
  ///--------------------------------------   DECODE  --------------------------------------------------
  ///---------------------------------------------------------------------------------------------------

  List decode([Map option]) {
    this.option=Map.from(option??{});
    while (isLooping) {
      var dict = decodeNext();
      skipSpace();
      if (dict != null) {
        dicts.add(dict);
        continue;
      }
      pos++;
    }

    var _links = [];
    links.forEach((key, element) {
      if (element.has(KeyLength)) {
        var id = element[KeyLength];
        if(id is String){
          element[KeyLength] = links[id][KeyRoot];
          _links.add(id);
        }
      }
    });
    _links.forEach(links.remove);

    var _dicts = [];
    for (var dict in dicts) {
      if (!(dict.has(KeyIndirect) && _links.contains(dict[KeyIndirect]))) {
        _dicts.add(dict);
      }
    }
    return _dicts;
  }

  bool isCmd(String code, {bool plus = true,bool shiftTop = false, bool shiftBot = false}) {
    if (shiftTop) skipSpace();
    if (bytes.isContains(code, pos)) {
      if(plus) pos += code.length;
      if (shiftBot) skipSpace();
      return true;
    }
    return false;
  }

  bool isOpen() {
    var _pos = pos;
    if (isCmd('<<') || isCmd('[') || isCmd('(') || isCmd('<')) {
      pos = _pos;
      return true;
    }
    return false;
  }

  dynamic decodeNext() {
    skipSpace();
    var objects = <String, dynamic>{};
    if (isIndirect(true)) {
      var id = lastIndirect;
      objects[KeyIndirect] = id;
      if (isOpen()) {
        var info = decodeNext();
        if (info is Map) {
          objects.addAll(info);
        } else {
          objects[KeyRoot] = info;
        }
      }

      ///parse stream
      if (isCmd('stream', shiftBot: true, shiftTop: true)) {
        var _pos = pos;
        buffering(() => isCmd('endstream'));
        var _poe = pos - 10;
        objects[KeyStream] = [_pos, _poe];
        skipSpace();
      } else if (isLooping&&isNum) {
        var n = decodeInt();
        if (n >= 0) objects[KeyRoot] = n;
      }
      var key = buffering(() => isCmd('endobj'), shiftTop: true);
      if (key.isNotEmpty) objects['/Extra'] = key.length;
      links[id] = objects;
      return objects;
    }

    if (isCmd('startxref')) {
      objects[KeyType] = '/StartXRef';
      objects[KeyRoot] = decodeInt();
      return objects;
    }

    if (isCmd('xref')) {
      objects[KeyType] = '/XRef';
      objects[KeyRoot] = decodeXrefTable(0, -1);
      return objects;
    }

    if (isCmd('trailer')) {
      objects[KeyType] = '/Trailer';
      objects.addAll(decodeNext());
      return objects;
    }

    if (isCmd('%')) {
      objects[KeyType] = '/Comment';
      var buffer = buffering(() => isSpaceBr);
      objects[KeyRoot] = buffer.bytesToString();
      return objects;
    }

    /// Dictionary

    if (isCmd('<<', shiftBot: true)) {
      while (isLooping) {
        skipSpace();
        if (isCmd('>>')) break;
        var key = decodeKey(true);
        if (isOpen()) {
          objects[key] = decodeNext();
        } else {
          objects[key] = decodeValue();
        }
      }
      if (objects.containsKey('%')) objects.remove('%'); //if % occur error !!!
      return objects;
    }

    /// Array
    if (isCmd('[', shiftBot: true)) {
      var lt = <dynamic>[];
      while (isLooping) {
        skipSpace();
        if (isCmd(']')) break;
        if (isOpen()) {
          lt.add(decodeNext());
        } else {
          lt.add(decodeValue());
        }
      }
      return lt;
    }

    /// String
    if (isCmd('(', shiftBot: true)) {
      var lt = StringBuffer();
      while (isLooping) {
        skipSpace();
        if (isCmd(')')) break;
        if (isOpen()) {
          lt.write(decodeNext());
        }else{
          lt.writeCharCode(byte);
          pos++;
        }
      }
      return '(${lt.toString()})';
    }

    /// Binary String
    if (isCmd('<')) {
      var buffer = buffering(() => isCmd('>'));
      return '<${buffer.bytesToString()}>';
    }

    return null;
  }

  List<dynamic> decodeXrefTable(int idx, int n) {
    var entries = <dynamic>[];
    while (n > 0) {
      var offset = decodeInt();
      if (offset == -1) break;
      var gen = decodeInt();
      if (gen == -1) break;
      var flag = decodeKey();
      var line = '${idx++},$offset,$gen,$flag';
      entries.add(line);
      n--;
    }
    skipSpace();
    if (isNum) {
      var _pos = pos;
      var idx = decodeInt();
      var n = decodeInt();
      if (idx >= 0 && n > 0) {
        entries.addAll(decodeXrefTable(idx, n));
      } else {
        pos = _pos;
      }
    }
    return entries;
  }

  num decodeInt() {
    var _pos = pos;
    var buffer = buffering(() => !isNum, shiftTop: true);
    if (buffer.isEmpty) {
      pos = _pos;
      return -1;
    }
    return num.parse(buffer.bytesToString());
  }

  dynamic decodeKey([bool skip = false]) {
    if (skip) skipSpace();
    var buffer = <int>[];
    if (isCmd('/')) buffer.add(ByteSlash);
    buffer += buffering(() => isSpaceAll || isDelimiter);
    if (skip) skipSpace();
    if (buffer.isNumbers) {
      return num.parse(buffer.bytesToString());
    }
    return buffer.bytesToString();
  }

  dynamic decodeValue() {
    if (isIndirect()) return lastIndirect;
    return decodeKey();
  }

  bool isIndirect([bool isObj = false]) {
    skipSpace();
    lastIndirect = null;
    if (isNum) {
      var _pos = pos;
      var oid = decodeInt();
      var gen = decodeInt();
      if (oid >= 0 && gen >= 0 && isCmd(' ${isObj ? 'obj' : 'R'}')) {
        lastIndirect = '$oid $gen R';
        if (isObj) skipSpace();
        return true;
      }
      pos = _pos;
    }
    return false;
  }

  void skipSpace() => buffering(() => !isSpaceAll);

  List<int> buffering(dynamic fun, {bool shiftTop = false, bool shiftBot = false}) {
    if (shiftTop) skipSpace();
    var buffer = <int>[];
    while (isLooping) {
      if (fun()) break;
      buffer.add(byte);
      pos++;
    }
    if (shiftBot) skipSpace();
    return buffer;
  }

  ///---------------------------------------------------------------------------------------------------
  ///--------------------------------------   ENCODE  --------------------------------------------------
  ///---------------------------------------------------------------------------------------------------

  List<int> encode([Map option]) {
    if (dicts.isEmpty) decode(option);
    this.option=Map.from(option??{});


    dicts.forEach((element) {
      if (element.has('/Type', '/Trailer')) {
        // keep the last trailer
        element.forEach((key, value) {
          if (key == '/Prev') return;
          trailer[key] = value;
        });
      }
      if (element.has(['/Comment', '/'])) {
        var comment = element['/'].toString();
        if (comment.startsWith(RegExp('^PDF-1.'))) version = comment;
      }
    });

    ///----start building

    var xref = <int, List<int>>{};
    var offsets = [];
    links.forEach((key, info) {
      if (info.has('/Type', '/XRef') || info.has('/Linearized')) return;
      if(trailer.containsKey('/Info')&&trailer['/Info']==key){
        this.info.forEach((key, value) {
          info[key]=value;
        });
        this.info=info;
      }

      var id = int.parse(key.split(' ').first);
      offsets.add(id);
      xref[id] = encodeObject(key, info);
    });

    print('info:${info}');

    offsets.sort();

    output.clear();
    output.writeStr('%$version\n');
    output.writeStr('%${hex.encode('AlmBBYS'.codeUnits)}\n');

    var xrefTable = '';
    xrefTable += '0 1\n';
    xrefTable += '0000000000 65535 f\n';
    for (var k in offsets) {
      xrefTable += '$k 1\n';
      xrefTable += '${output.length.toString().padLeft(10, '0')} 00000 n\n';
      output.addAll(xref[k]);
    }
    var startxref = output.length;
    output.writeStr('xref\n');
    output.writeStr(xrefTable);
    output.writeStr('trailer\n');
    trailer['/Size'] = offsets.length;
    output.writeStr('${encodeNext(trailer)}\n');
    output.writeStr('startxref\n');
    output.writeStr('$startxref\n');
    output.writeStr('%%EOF\n');
    return output;
  }

  List<int> encodeObject(String key, Map info) {
    var stream =encodeProcess(bytes.rawStream(info), info);

    var object = <int>[];
    object.writeStr('${key.replaceAll('R', 'obj')}\n');
    if (info.has(KeyIndirect)) info.remove(KeyIndirect);

    if (info.has(KeyRoot) && info.length == 1) info = info[KeyRoot];
    object.writeStr('${encodeNext(info)}\n');
    if (stream.isNotEmpty) {
      object.writeStr('stream\n');
      object.addAll(stream);
      object.writeStr('\nendstream\n');
    }
    object.writeStr('endobj\n');
    return object;
  }

  dynamic encodeNext(dynamic input) {
    var lines = <String>[];
    if (input is Map) {
      input.forEach((key, value) {
        lines.add('$key ${encodeNext(value)}');
      });
      return '<<${lines.join()}>>';
    } else if (input is List) {
      input.forEach((value) {
        lines.add(encodeNext(value));
      });
      return '[${lines.join(' ')}]';
    }
    return input.toString();
  }

  List<int> encodeProcess(List<int> stream, Map info) {
    if (option.has('image')&&info.has(['/Length', '/Width', '/Height', '/Type', '/Subtype']) &&  info.has('/Subtype', '/Image')) {
      var imageOp=Map.from(option['image']);
      if(imageOp.has(['wide','limit','quality'])){
        int w = info['/Width'];
        int h = info['/Height'];
        if (w * h > imageOp['limit']) {
          String cs = info['/ColorSpace'] ?? info['/CS'];
          String filter = info['/Filter'];
          var format = Format.rgba;
          switch (cs) {
            case '/DeviceGray':
              format = Format.luminance;
              break;
            case '/DeviceCMYK':
              format = Format.rgba;
              break;
            case '/DeviceRGB':
              format = Format.rgb;
              break;
          }
          var ratio = w / h;
          var nw = imageOp['wide'];
          var nh = (nw / ratio).round();
          print('input->$info');
          var deStream=stream.decodeStream(info);
          Image inImage;
          if(filter=='/DCTDecode'){
            inImage=decodeImage(deStream);
          }else{
            inImage=Image.fromBytes(w, h, deStream, format: format);
          }
          inImage=copyResize(inImage, width: nw, height: nh);
          var output=<int>[];
          if(filter=='/DCTDecode'){
            output =encodeJpg(inImage,quality: imageOp['quality']);
          }else{
            output=zlib.encode(inImage.getBytes(format: format));
          }
          info['/BitsPerComponent'] = 8;
          info['/Length'] = output.length;
          info['/Filter'] = filter;
          info['/Width'] = nw;
          info['/Height'] = nh;
          print('output->$info');
          return output;
        }
      }
    }
    return stream;
  }
}

extension _dynamic on dynamic {
  void writeStr(String input) => this.addAll(input.codeUnits);

  String bytesToString() => String.fromCharCodes(List<int>.from(this));

  List<int> rawStream(Map info) {
    var dat = <int>[];
    if (info.has(Pack.KeyStream)) {
      var bytePos = List.from(info[Pack.KeyStream]);
      info.remove(Pack.KeyStream);
      dat = this.sublist(bytePos.first, bytePos.last);
    }
    return dat;
  }


  List<int> decodeStream(Map info) {
    if (info.has('/Filter', '/FlateDecode')) {
      int predictor = 1;
      int width = 1;
      int bpc = 8;
      int colors = 1;
      if (info.has('/DecodeParms')) {
        width = info['/DecodeParms']['/Columns'];
        predictor = info['/DecodeParms']['/Predictor'];
      }
      return flateDecode(this, predictor: predictor, width: width, bpc: bpc, colors: colors);
    }
    return this;
  }

  bool get isNumbers {
    var val = this;
    var res = 0;
    var len = val.reqLength;
    this.forEach((element) {
      if ('0123456789.'.codeUnits.contains(element)) res++;
    });
    return res != 0 && res == len;
  }

  bool isContains(String code, int start) {
    var codes = code.codeUnits;
    var end = start + codes.length;
    if (end <= this.reqLength && codes.equalsWith(this.sublist(start, end))) {
      return true;
    }
    return false;
  }

  bool equalsWith(List<int> list2) {
    List<int> list1 = this;
    if (list1 == null || list2 == null) return false;
    var length = list1.length;
    if (length != list2.length) return false;
    for (var i = 0; i < length; i++) {
      if (list1[i] != list2[i]) return false;
    }
    return true;
  }
}

String ct(String input,[String jo=' ']){
  return input.split(jo).join();
}